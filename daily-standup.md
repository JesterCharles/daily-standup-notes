# 9/13 Group 1 - Standup

-   Sandra

    -   Currently, have the wedding planner completed. All testing for wedding-planner worked, attempting postman requests and test they are working and begin implmeneting into the frontend.

-   Ash

    -   Currently, completed the backend wedding planner. Front end address 2/3 pages. Authorization page requires debugging, connection issues with front end. Moving to messing and file upload. Attempting to complete theses backends and debug.

-   Michael

    -   Currently, on back-end wedding planner. Able to create weddings, weddingbyID, get all weddings. Issues with update wedding ID by idea. Testing with postgresQL. Issues with update and deletion for backend.

-   Donavan

    -   Currently, completed wedding-planner, authorization. Need frontend for authorization. After, need messing and authorizaiton services. No issues YET

-   Charles
    -   Currently, have the wedding-planner completed, excluding expenses on front end. Authorizaiton is fully implemented and also the file upload. Chat services and CSS.

# 9/14 Group 1 - Stand Up

-   Sandra

    -   Began react, working on building out the planner page for wedding and expense. Worked on incorporating some CSS. Errors on postman after working, potentially server isn't up and running.

-   Ash

    -   File upload completed on fron and back end. Cocentrating on front-end posting, updating and deleting. Issues with react attempting to assemble the components, reverting from grid approach.

-   Donovan

    -   Upload front and back-end completed. Working on login authorization and having issues passing credentials with credentials between components. Potentially Link to help solve this.

-   Michael

    -   Wedding-planner servives are now able to complete updates by wedding ID working. Deletion comes up with does not exist consistently. Began working on React and attempting some simple requests.

# 9/15 Group 1 - Stand Up

-   Sandra

    -   Got all the GET requests integrated with front end for wedding planner service.
    -   Creation form in progress
    -   Agenda For today: Implement the authorization service

-   Ash

    -   Worked on react front end for wedding planner service for editing table and posting requests for "weddings"
    -   Need to do the same for expenses
    -   Agenda for today: Will try to get messaging service started today

-   Donovan

    -   Problems: - Working on image upload. file didnt get uploaded correctly - Login working through postman, but integration with front end resulting in bugs
    -   Agenda for today: Need to fix both and start working on messaging service

-   Michael

    -   Worked and completed Wedding planner service backend APIs
    -   Currently Working on - React , Authorization currently and trying to get that done

-   Charles
    -   currently finished all of wedding planner, authorization, and file upload
    -   Agenda for today: Need to begin messaging service and CSS

# 9/16 Daily Stand up

-   Sandra
    -   **Current:** Made lots of progress towards wedding planner. Cleaning up React code
    -   **Errors:** Had issues connecting to the compute engine for the backend. Potentially didn't update URL or allow access to the compute engine instance.
    -   **To Do:** Finalizing the wedding-planner on front end and working on file upload.
-   Ash
    -   **Current**: Cleaned up a lot of code to optimize a lot of code and insure there is no bugs.
    -   **To Do:** Messaging Service all that's left to do.
-   Donavan
    -   **Current:** Finished file upload and authorization, front and backend.
    -   **Errors:** Attempted tokens, but desided to scrap it until after everything else is done.
    -   **To Do**: Finalizing the CSS and Messaging service
-   Michael
    -   **Current:** Complete the postman testing for wedding planner. Began the react frontend for wedding planner.
    -   **To Do:** Finalizing the wedding planner on front end. Beginning other services.
-   Charles
    -   **Current**: Re-established progress lost from computer failure, cleaned up code and added all errors displaying on frontend with alerts
    -   **To Do:** Begin messaging services and work on CSS.

# 9/17 Daily Stand up

-   Sandra
    -   **Current:** Almost completed with planner page. Deletes for wedding and expenses.
    -   **Errors:** Issues with weddingID grabbing the approriate value. Data from get all expenses and weddings seem to be limited
    -   **To Do:** Getting wedding planner polished and completed. Once squared away, will work on authorizaiton.
-   Ash
    -   **Current:** Got my GET requests working and trying to work on front end for messaing service.
    -   **To Do:** After that's completed, she will work on post .
-   Donavan
    -   **Current:** Messing backend is completed, briefly started the frontend.
    -   **Errors:** Having issues with map function not working when generating table. This is probably due to information reading, might just be an obj or string. Comment out the map and console log to see what you're feeding in the table.
    -   **To Do:** Put wedding planner on compute engine and finalize the frontend messaging service.
-   Michael
    -   **Current:** Formatted planner page and working with authorization services that is successful with postman.
    -   **Errors:** Authorization has errors when trying to utilize the app engine instance.
    -   **To Do:** Work on finalizing the authorization page and planner to be presentation ready.
-   Charles
    -   **Current:** Messaging backend portion and have gitlab.
    -   **To Do:** Messaging frontend, Deploying our wedding planner compute engine and good ol' CSS.
